# 在多页面项目下使用Webpack+Vue loader

---


## 项目目录结构

    ├─html (线上用户访问的.html目录)
    │   └─index (生成的一个模块）
    │       ├─index.html (同一模块的模板放在一个模块目录下)
    │       └─xxx.html
    │
    │
    ├─dist (线上资源文件目录)
    │  ├─css
    │  ├─imgs
    │  ├─js
    │  └─...
    └─src (前端开发目录)
        ├─pageOne (一个业务模块,每个业务下可能有多个页面)
        │  └─index
        │      ├─app.vue
        │      ├─index.html
        │      ├─index.js
        │      └─static (资源文件)
        ├─components (vue组件目录)
        │  ├─A
        │  │ ├─A.vue
        │  │      
        │  └─B
        │    ├─B.vue
        │          
        └─pageTwo (一个业务模块,每个业务下可能有多个页面)
            ├─index
            │  ├─app.vue
            │  ├─index.html
            │  ├─index.js
            │  └─static
            └─xxx
               └─xxx.html

## 页面文件

每个页面都是一个文件夹，所需的资源文件也都放在这个文件夹下，不需要这个页面时，也只需要删除这个文件夹。


## 安装和编译
* `npm install`——安装依赖的包
* `bower install`——安装依赖的库
* `npm run build`或`webpack build`——打包js、css和静态资源到dist文件夹，按模块分组生成html文件到html文件夹
* `npm run dev`——启动webpack-dev-server服务，监听8080端口，可以通过"http://localhost:8080/"查看页面效果






